spec MY-DIFFIE-HELMAN is
Theory
  types Name Nonce MultipliedNonces Generator Exp Key GeneratorOrExp Secret .
  subtypes Generator Exp < GeneratorOrExp .
  subtypes Exp < Key .
  subtypes Name Generator < Public .
  subtype Nonce < MultipliedNonces .

  ops a b i : -> Name .
  ops e d : Key Msg -> Msg .

  op _;_ : Msg Msg -> Msg [gather (e E)] .
  op _*_ : MultipliedNonces MultipliedNonces -> MultipliedNonces [assoc comm] .

  op g : -> Generator .

  op exp : GeneratorOrExp MultipliedNonces -> Exp .

  op n : Name Fresh -> Nonce .

  op sec : Name Fresh -> Secret .

  var G : Generator .
  vars Y Z : MultipliedNonces .
  eq exp(exp(G,Y),Z) = exp(G,Y * Z) .

Protocol
  vars AName BName A1Name : Name .
  vars r1 r2 r3 : Fresh .
  vars XEA XEB : Exp .
  var S : Secret .

  roles A B .

  In(A) = AName, BName .
  In(B) = BName .

  1 . A -> B : AName ; BName ; exp(g,n(AName,r1)) |- A1Name ; BName ; XEB .

  2 . B -> A : A1Name ; BName ; exp(g,n(BName,r2)) |- AName ; BName ; XEA .

  3 . A -> B : e(exp(XEA,n(AName,r1)), sec(AName,r3)) |- e(exp(XEB,n(BName,r2)),S) .

  Out(A) = exp(XEA,n(AName,r1)) .
  Out(B) = exp(XEB,n(BName,r2)) .

Intruder
  var NAME : Name .
  var K : Key .
  vars M M1 M2 : Msg .
  vars NS1 NS2 : MultipliedNonces .
  var GE : GeneratorOrExp .
  var r  : Fresh .
           => n(i, r), g, NAME .
  M1 ; M2 <=> M1, M2 .
  K, M     => e(K, M), d(K, M) .
  NS1, NS2 => NS1 * NS2 .
  GE, NS1  => exp(GE, NS1) .

Attacks
0 .
    B executes protocol .
    Subst(B) = A1Name |-> a, BName |-> b, S |-> sec(a, r3) .
    without:
        Subst(A) = AName |-> a, BName |-> b .
        A executes protocol .
ends
