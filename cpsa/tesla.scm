(herald "TESLA Broadcast Authentication Protocol")

(defprotocol tesla basic
  (defrole bcast
    (vars (mj text) (k1 k0 kmac-1 kmac-2 skey) (v0 v1 data))
    (trace
     (send (cat mj (enc mj kmac-1) k0))
     (obsv (cat v0 v1))
     (send (cat mj (enc mj kmac-2) k1)))
     (eq k1 (key-of v1))
     (lt k0 k1))
  (defrole resp
    (vars (mj text) (k1 k0 kmac-1 kmac-2 skey) (v data))
    (trace
     (recv (cat mj (enc mj kmac-1) k0))
     (obsv (cat v0 v1))
     (recv (cat mj (enc mj kmac-2) k1))
     (eq k1 (key-of v1))
     (lt k0 k1)))
  (defrole initialize
    (vars (v v0 data) (key0 skey))
    (trace (init (cat v v0)))
    (uniq-gen v)
    (fn-of (key-of (v0 key0))))
  (defrole successor
    (vars (v v0 data) (key0 key skey))
    (trace (tran (cat v v0) (cat v v0 (hash v0))))
    (fn-of (key-of (v0 key0) ((hash v0) key)))
    (lt key0 key)))

(defskeleton tesla
  (vars (mj text) (k1 k0 kmac-1 kmac-2 skey))
  (defstrand bcast 2 (mj mj) (k0 k0) (k1 k1) (kmac-1 kmac-1) (kmac-2 kmac-2)))


(defskeleton tesla
  (vars (mj text) (k1 k0 kmac-1 kmac-2 skey))
  (defstrand resp 2 (mj mj) (k0 k0) (k1 k1) (kmac-1 kmac-1) (kmac-2 kmac-2)))
